#ifndef LIBEBM_HPP_
#define LIBEBM_HPP_


#include <libcaer/events/polarity.h>
#include <iostream>
#include <atomic>
#include <csignal>
#include <cmath>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */

#define RADIUS 37
#define SIZEX 240
#define SIZEY 180


typedef struct posCircle
{
	double x, // x-position
		  y, // y-position
		  u, // x-speed
		  v, // y-speed
		  a, // acc-x
		  b, //	acc-y
		  timestamp; // timestamp of the position
} posCircle;


typedef struct eventType
{
	double  timestamp; 
	double posx, posy;
    bool onoff;
}eventType;

using namespace std;


/*
** Sort a vector with the last position of the circle and delete the events that are out of a ring
** 
** in -> event
** pos-> last position of circle
*/
bool isEventInCircle(eventType &in, posCircle pos);


/*
** Read events from csv file with the format [x;y;timestamp;polarity]
*/
vector<eventType> readEventsFromCSV(string path);

/*
** Increase the value of the hough point in the array
*/
void increaseHoughPoint(u_int32_t x, u_int32_t y, float weight,u_int32_t& maxX, u_int32_t& maxY,u_int32_t& maxValue, u_int32_t** accumulatorArray);

/*
** This algorithm is copied from the jAER Project of the company Inivation and adapted for C++
** Fast inclined ellipse drawing algorithm; ellipse eqn: A*x^2+B*y^2+C*x*y-1 = 0
** the algorithm is fast because it uses just integer addition and subtraction
*/
void accumulate(eventType event, float weight, u_int32_t& maxX, u_int32_t& maxY,u_int32_t& maxValue, u_int32_t** accumulatorArray);

/*
** Generate hough with a vector of events
** 
** in -> vector of events to generate hough
** maxX -> variable to save max of in X-direction
*/
void houghCircle(vector<eventType> in,u_int32_t& maxX,u_int32_t& maxY,bool writeToFile);

/*
** Generate vector of events from eventPacket
** 
** pPolPack -> PolarityEventPacket from Camera or File
** packetSize -> size of EventPacket
*/
vector<eventType> getVectorFromEventPacket(caerPolarityEventPacket pPolPack,u_int32_t packetSize);

/*
** Search for smallest timestamp in  a vector of events
** 
** data -> events as vector
*/
const double getTimeMin(vector<eventType> data);

/*
** Search for biggest timestamp in a vector of events
** 
** data -> events as vector
*/
const double getTimeMax(vector<eventType> data);

/*
** Generate PolarityEventPacket from vector
** 
** sortEvVect -> Vector with events from Type eventType
*/
caerPolarityEventPacket generatePolarityEventPacket(vector<eventType> sortEvVect);

/*
** Fitting a cylinder in a vector of events with gradient descent.
** Optimized calculations to make algorithm fast enough.
** 
** data -> converted events from PolarityEventPacket to a vector -> getXYT
** pos -> last calculated position
*/
posCircle cylinderFitting(vector<eventType> &data, posCircle pos);


#endif
