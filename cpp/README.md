libebm
====

C++ library - event based algorithms

REQUIREMENTS:

LINUX

pkg-config

cmake >= 3.0

gcc >= 4.9 or clang >= 3.6

libusb >= 1.0.17

libcaer >= 2.5.0


Please make sure that you have the various development packages installed
for the above dependencies. They are usually called PKG-dev or PKG-devel.

INSTALLATION:

1) configure:

$ cmake -DCMAKE_INSTALL_PREFIX=/usr .

2) build:

$ make

3) install:

$ sudo make install
