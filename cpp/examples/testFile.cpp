#include <atomic>
#include <csignal>
#include <iostream>
#include <libebmcpp/libebm.hpp>
#include <libcaer/events/polarity.h>
#include <vector>
#include <sstream>
#include <fstream>
#include <string>
#include <ctime>
#include <sys/time.h>
#include<cmath>
using namespace std;



eventType tmpEv;
std::vector<eventType> tmpEvVect,sortEvVect;

int main(void) {
	//read events from csv file

	tmpEvVect=readEventsFromCSV(""); //filepath

	//create a eventPacket to load the events
	caerPolarityEventPacket pPolPack;

	//get minTimestamp
	u_int32_t firstTimestamp=getTimeMin(tmpEvVect);
	u_int32_t maxX=0, maxY=0;
	sortEvVect.clear();

	const u_int32_t ms=1000;

	const u_int32_t minSize=200;
	
	// search for Events in the first ms
	for (u_int32_t vIdx=0; vIdx<tmpEvVect.size(); vIdx++)
	{
		if(tmpEvVect[vIdx].timestamp<=(firstTimestamp+ms) && firstTimestamp<=tmpEvVect[vIdx].timestamp){
				sortEvVect.push_back(tmpEvVect[vIdx]);
		}
	}

	//hough
	houghCircle(sortEvVect,maxX,maxY,false);

	// define startposition of cylinderfitting
	posCircle pos;
	pos.x=maxX;
	pos.y=maxY;
	pos.v=0;
	pos.u=0;
	
	cout<<"Hough-MAX: X: "<<pos.x<<" Y: "<<pos.y<<endl;

	// file to write results
	ofstream myfile,myfile1;
	myfile.open ("output.csv");
	sortEvVect.clear();

	// full time
	long endtime=0;
	bool end=false;

	
	int t=0;
	int size=0;
	vector<eventType> tmp;
	bool test=false;
	int r=0;

    	while (true){
    		tmp.clear();
    		test=false;
    		for (u_int32_t vIdx=0; vIdx<tmpEvVect.size(); vIdx++)
    		{
			//find all Events in the next ms
    			if(tmpEvVect[vIdx].timestamp<=(firstTimestamp+ms) && firstTimestamp<=tmpEvVect[vIdx].timestamp){
				// just add the Event if it is in a ring around the last position
    				if(isEventInCircle(tmpEvVect[vIdx],pos))
    					tmp.push_back(tmpEvVect[vIdx]);
					//at the end of vector break
					if(vIdx==(tmpEvVect.size()-1)){
						end=true;
					}
    			}
    		}
		//if there are enough Events in the ms we can clear the vector and add the other
    		if(tmp.size()>minSize){
    			sortEvVect.clear();
    		}else{
		//if there aren't enough Events in the ms we use the Events from the ms before
    			if(sortEvVect.size()>minSize)
    				sortEvVect.erase(sortEvVect.begin(),sortEvVect.begin()+((sortEvVect.size()-minSize)+tmp.size()));
    		}
		//add the events of the ms
		for (u_int32_t vIdx=0; vIdx<tmp.size(); vIdx++)
		{
			sortEvVect.push_back(tmp[vIdx]);
		}
		if(sortEvVect.size()!=0){
			// some variables to calculate the duration of the calculation
			struct timeval start, end;
			long mtime, seconds, useconds;

			gettimeofday(&start, NULL);
			
			//start cylinderfitting algorithmus with the last position
			pos=cylinderFitting(sortEvVect, pos);
			
			gettimeofday(&end, NULL);

			seconds  = end.tv_sec  - start.tv_sec;
			useconds = end.tv_usec - start.tv_usec;

			mtime = ((seconds) * 1000000 + useconds);
			endtime=endtime+mtime;
			cout<<"Elapsed time in useconds: "<<mtime<<" ENDTIME: "<<endtime<<"X: "<<pos.x<<"Y: "<<pos.y<<endl;
			
			
			// write to file
			myfile.precision(5);
			myfile.setf(ios::fixed);
			myfile.setf(ios::showpoint);
			myfile<<pos.x<<";"<<pos.timestamp<<";"<<endtime<<";"<<mtime<<";"<<tmp.size()<<"\n";
    		}


		// add 1ms to timestamp
	    	firstTimestamp=firstTimestamp+ms;
	    	myfile.flush();

	    	// end the programm
	    	if (end)
	    		break;
    }

    myfile.close();
    return 0;
}

