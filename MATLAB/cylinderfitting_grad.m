%% Function Name: Cylinder fitting with linear guide curve
%
%
% Inputs:
%   input_args -> array with three columns (x,y,t)
%   x_max -> last position in x-direction 
%   y_max -> last position in y-direction 
%   u0 -> last velocity of the algorithm in x-direction  
%   v0 -> last velocity of the algorithm in y-direction
%
% Outputs:
%   x0 -> x-startposition of the cylinder
%   y0 -> y-startposition of the cylinder
%   timestamp0 -> t-startposition of the cylinder
%   x1 -> x-endposition of the cylinder
%   y1 -> y-endposition of the cylinder
%   timestamp1 -> t-endposition of the cylinder
%   u0 -> new velocity of the algorithm in x-direction  
%   v0 -> new velocity of the algorithm in y-direction
%
% $Date: February, 2019 @ Hannes Erlacher
% ________________________________________


function [ x0,y0,timestamp0,x1,y1,timestamp1,u0,v0] = cylinderfitting_grad( input_args ,x_max,y_max,u0,v0)
    % split input args x,y,t
    x=input_args(:,1);
    y=input_args(:,2);
    % scale time to ms that the algorithms runs stably
    t=(input_args(:,3)-min(input_args(:,3)))*1/1000;
    
    % set the startpositions
    x0=x_max;
    y0=y_max;
    
    % radius in px
    r=37;
    
    check=true;
    
    sizex=size(x);
    s=zeros(1,sizex(1,1));
    
    % search time min and max
    timestamp0=min(input_args(:,3));
    timestamp1=max(input_args(:,3));
    
    while check
        dx0=0;dy0=0;du0=0;dv0=0;
        k=k+1;
        for i=1:sizex(1,1)
            s(i)=(x(i)-x0-t(i)*u0)^2 +(y(i)-y0-t(i)*v0)^2-r^2;
        end
        for i=1:sizex(1,1)
            dx0=dx0+s(i)*2*(x(i)-x0-t(i)*u0);
            dy0=dy0+s(i)*2*(y(i)-y0-t(i)*v0);
            dy0=dy0+s(i)*2*(y(i)-y0-t(i)*v0);
            du0=du0+s(i)*2*(x(i)-x0-t(i)*u0)*t(i);
            dv0=dv0+s(i)*2*(y(i)-y0-t(i)*v0)*t(i);
        end
        alpha=6.2e-10;
        x0=x0+alpha*dx0;
        y0=y0+alpha*dy0;
        u0=u0+alpha*du0;
        v0=v0+alpha*dv0;
        % check the precision of the gradients
        if (abs(dx0)< 50000) && (abs(dy0)< 50000) && (abs(du0)< 50000) && (abs(dv0)< 50000)
              check=false;
        end
    end
    x1=x0+(timestamp1-timestamp0)*u0/1000;
    y1=y0+(timestamp1-timestamp0)*v0/1000;
    
%   plot results
    
%   figure
%   scatter3(x,y,t*1000,'s','filled')
%   hold on  
%   circle(x0,y0,r,0)
%   for t=0:25:3000
%      circle(x0-t*u0,y0-t*v0,r,t)
%   end
%   circle(x1,y1,r,(timestamp1-timestamp0))
    

