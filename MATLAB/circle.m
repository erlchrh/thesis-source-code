%% Function Name: Function to draw a circle
%
%
% Inputs:
%   x and y are the coordinates of the center of the circle
%   r is the radius of the circle
%
% $Date: February, 2019 @ Hannes Erlacher
% ________________________________________


function circle(x,y,r,t)
ang=0:0.01:2*pi; 
xp=r*cos(ang);
yp=r*sin(ang);
plot3(x+xp,y+yp,ones(size(ang))*t,'k-')
end