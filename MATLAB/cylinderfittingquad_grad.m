%% Function Name: Cylinder fitting with square guide curve
%
%
% Inputs:
%   input_args -> array with three columns (x,y,t)
%   x_max -> last position in x-direction 
%   y_max -> last position in y-direction 
%   u0 -> last velocity of the algorithm in x-direction  
%   v0 -> last velocity of the algorithm in y-direction
%   a0 -> last acceleration of the algorithm in x-direction
%   b0 -> last acceleration of the algorithm in y-direction
%
% Outputs:
%   x0 -> x-startposition of the cylinder
%   y0 -> y-startposition of the cylinder
%   timestamp0 -> t-startposition of the cylinder
%   x1 -> x-endposition of the cylinder
%   y1 -> y-endposition of the cylinder
%   timestamp1 -> t-endposition of the cylinder
%   u0 -> new velocity of the algorithm in x-direction  
%   v0 -> new velocity of the algorithm in y-direction
%   a0 -> new acceleration of the algorithm in x-direction
%   b0 -> new acceleration of the algorithm in y-direction
%
% $Date: February, 2019 @ Hannes Erlacher
% ________________________________________




function [ x0,y0,timestamp0,x1,y1,timestamp1,u0,v0,a0,b0] = cylinderfittingquad_grad( input_args ,x_max,y_max,u0,v0,a0,b0)
    % split input args x,y,t
    x=input_args(:,1);
    y=input_args(:,2);
    % scale time to ms that the algorithms runs stably
    t=(input_args(:,3)-min(input_args(:,3)))*1/1000;
    
    % set the startpositions
    x0=x_max;
    y0=y_max;
   
    % radius in px
    r=37;
    
    check=true;
    
    sizex=size(x);
    s=zeros(1,sizex(1,1));
    
    % search time min and max
    timestamp0=min(input_args(:,3));
    timestamp1=max(input_args(:,3));
    
    while check
        dx0=0;dy0=0;du0=0;dv0=0;da0=0;db0=0;
        for i=1:sizex(1,1)
            s(i)=(x(i)-x0-t(i)*(u0+(t(i)/2)*a0))^2 +(y(i)-y0-t(i)*(v0+(t(i)/2)*b0))^2-r^2;
        end
        for i=1:sizex(1,1)
            dx0=dx0+s(i)*2*(x(i)-x0-t(i)*(u0+(t(i)/2)*a0));
            dy0=dy0+s(i)*2*(y(i)-y0-t(i)*(v0+(t(i)/2)*b0));
            du0=du0+s(i)*2*(x(i)-x0-t(i)*(u0+(t(i)/2)*a0))*t(i);
            dv0=dv0+s(i)*2*(y(i)-y0-t(i)*(v0+(t(i)/2)*b0))*t(i);
            da0=da0+s(i)*(x(i)-x0-t(i)*(u0+(t(i)/2)*a0))*t(i)*t(i);
            db0=db0+s(i)*(y(i)-y0-t(i)*(v0+(t(i)/2)*b0))*t(i)*t(i);
        end
        alpha=(1/(40000000000));
        x0ld=x0;
        y0ld=y0;
        u0ld=u0;
        v0ld=v0;
        a0ld=a0;
        b0ld=b0;
        x0=x0+alpha*dx0;
        y0=y0+alpha*dy0;
        u0=u0+alpha*du0;
        v0=v0+alpha*dv0;
        a0=a0+alpha*da0;
        b0=b0+alpha*db0;
        % check the precision between the old and new values
        if (abs(x0ld-x0)< 0.0001) && (abs(y0ld-y0)< 0.0001) && (abs(u0ld-u0)< 0.0001) && (abs(v0ld-v0)< 0.0001) && (abs(a0ld-a0)< 0.0001) && (abs(b0ld-b0)< 0.0001)
              check=false;
        end
    end
    x1=x0+(timestamp1-timestamp0)*u0/1000+((timestamp1-timestamp0)^2)/2*a0/(1000^2);
    y1=y0+(timestamp1-timestamp0)*v0/1000+((timestamp1-timestamp0)^2)/2*b0/(1000^2);
     
%   plot results

%   figure
     
%   scatter3(x,y,t*1000,'gs','MarkerEdgeColor','k','MarkerFaceColor','r')
%   hold on  
%   circle(x0,y0,r,0)
%   c=1;    
%   for t=0:5:(timestamp1-timestamp0)
%          x2(c)=x0+t*u0/1000+(t^2)/2*a0/(1000^2);
%          y2(c)=y0+t*v0/1000+(t^2)/2*b0/(1000^2);
%          c=c+1;
%   end
%   t=0:5:timestamp1-timestamp0;
%   circle(x1,y1,r,(timestamp1-timestamp0))
%   plot3(x2,y2,t,'-');
%      
%   hold off;
     
     
    

